import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'; 
import Icons from 'react-native-vector-icons/FontAwesome'; 

export default class AboutScreen extends Component{
    render(){
        return (
            <View style={styles.container}>
                <ScrollView style={styles.container}>
                <Text style={{fontSize:36, marginTop:64, textAlign:'center', fontWeight:'bold'}}>Tentang Saya</Text>
                <View style={styles.imageProfile}>
                    <Icon name="md-person" color="#CACACA" size={150} />
                </View>
                <Text style={{fontSize:24, color:'#003366', fontWeight:'bold', paddingTop:24, textAlign:'center'}}>Agung Budi Miantoro</Text>
                <Text style={{fontSize:16, color:'#3EC6FF', fontWeight:'bold',paddingTop:10, textAlign:'center'}}>Agung Budi Miantoro</Text>
                <View style={styles.portofolio}>
                    <Text style={{fontSize:18, color:'#003366', margin:5}}>Portofolio</Text>
                    <View style={{height:0.5, backgroundColor:'#003366', marginVertical:3, marginHorizontal:8}} />
                    <View style={styles.portofolioKonten}>
                        <View style={styles.portofolioKontenIsi}>
                            <Icons name="gitlab" color='#3EC6FF' size={50} />
                            <Text style={{fontSize:16, fontWeight:'bold',color:'#003366', textAlign:'center', marginTop:5}}>@neonxioda</Text>
                        </View>
                        <View style={styles.portofolioKontenIsi}>
                            <Icon name="logo-github" color="#3EC6FF" size={50} />
                            <Text style={{fontSize:16, fontWeight:'bold',color:'#003366', textAlign:'center'}}>@agungbudimiantoro</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.HubungiSaya}>
                    <Text style={{fontSize:18, color:'#003366', margin:5}}>Hubungi Saya</Text>
                    <View style={{height:0.5, backgroundColor:'#003366', marginVertical:3, marginHorizontal:8}} />
                    <View style={styles.hubungiSayaKonten}>
                        <View style={styles.hubungiSayaKontenIsi}>
                            <Icon name="logo-facebook" color="#3EC6FF" size={50} />
                            <Text style={styles.hubungiSayaKontenIsiParagraf}>agung.miantoro</Text>
                        </View>
                        <View style={styles.hubungiSayaKontenIsi}>
                            <Icon name="logo-instagram" color="#3EC6FF" size={50} />
                            <Text style={styles.hubungiSayaKontenIsiParagraf}>@agung_budi_m</Text>
                        </View>
                    </View>
                </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    imageProfile:{
        backgroundColor:'#EFEFEF',
        width:200,
        height:200,
        marginHorizontal:'auto',
        borderRadius:100,
        marginTop:12,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center'
    },
    portofolio:{
        margin:8,
        backgroundColor:'#EFEFEF',
        width:359,
        height:140,
        borderRadius:16,
        marginTop:16,
        justifyContent:'center',
        alignSelf:'center'
    },
    HubungiSaya:{
        margin:8,
        backgroundColor:'#EFEFEF',
        width:359,
        borderRadius:16,
        marginTop:16,
        flex:1,
        minHeight:180,
        justifyContent:'center',
        alignSelf:'center'
    },
    portofolioKonten:{
       flexDirection:'row',
       justifyContent:'space-evenly'
    },
    portofolioKontenIsi:{
        alignItems:'center',
        marginVertical:17,
    },
    hubungiSayaKonten:{
        justifyContent:'center',
        alignItems:'center'
    }, 
    hubungiSayaKontenIsi:{
        flexDirection:'row',
        paddingTop:5
    },
    hubungiSayaKontenIsiParagraf:{
        fontSize:16, 
        fontWeight:'bold', 
        paddingTop:15 , 
        paddingHorizontal:18 ,
        color:'#003366', 
        textAlign:'center'
    }
  });