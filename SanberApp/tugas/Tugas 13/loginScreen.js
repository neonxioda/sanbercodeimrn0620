import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'; 
export default class LoginScreen extends Component{
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                  <Image source={require('./Images/logo.png')} style={{width:330, height:100,}} />
                  {/* <Image source={{uri:'https://sanbercode.com/logo-horizontal.png'}} style={{width:320, height:100}} /> */}
                </View>
                <View style={styles.loginContainer}>
                    <Text style={{fontSize:24, fontFamily:'Roboto' , textAlign:'center'}}>Login</Text>
                    <View style={styles.inputContainer}>
                        <Text style={{fontSize:16, fontFamily:'Roboto'}}>Username / Email</Text>
                        <TextInput style={styles.input} />
                        <Text style={{fontSize:16, fontFamily:'Roboto', marginTop:20}}>Password</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={styles.tombolContainer}>
                       <TouchableOpacity style={{backgroundColor:'#3EC6FF', width:140, height:40, borderRadius:16, marginHorizontal:'auto', marginVertical:20 , justifyContent:'center', alignItems:'center'}}>
                           <Text style={{color:'white', fontSize:24, fontFamily:'Roboto'}}>Login</Text>
                       </TouchableOpacity>
                       <Text style={{fontSize:24, fontFamily:'Roboto', color:'#3EC6FF', textAlign:'center'}}>atau</Text>
                       <TouchableOpacity style={{backgroundColor:'#003366', width:140, height:40, borderRadius:16, marginHorizontal:'auto', marginVertical:20 , justifyContent:'center', alignItems:'center'}}>
                           <Text style={{color:'white', fontSize:24, fontFamily:'Roboto'}}>Daftar ?</Text>
                       </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    logoContainer:{
       alignItems:'center',
       paddingTop:50,
       paddingBottom:50
    },
    loginContainer:{
        paddingTop:5,
        paddingHorizontal:20
    },
    inputContainer:{
        padding:20,
        marginTop:20
    },
    input:{
        width:'100%',
        height:40,
        borderColor:'#003366',
        borderWidth:1
    },
    tombolContainer:{
        justifyContent:'center',
        alignContent:'center',
        alignSelf:'center'
    }
  });