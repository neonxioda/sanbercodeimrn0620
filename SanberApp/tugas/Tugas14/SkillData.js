import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'; 

export default class videoItem extends Component{
  render(){
      let isi = this.props.isi;
  return (
    <View style={styles.container}>
        <Icon name={isi.iconName} size={100} color="#003366" />
        <View style={styles.content}>
            <Text style={{fontSize:24, fontWeight:'bold', color:'#003366'}}>{isi.skillName}</Text>
            <Text style={{fontSize:16, fontWeight:'bold', color:'#3EC6FF'}}>{isi.categoryName}</Text>
            <Text style={{fontSize:48, fontWeight:'bold', color:'#FFFFFF', textAlign:'right'}}>{isi.percentageProgress}</Text>
        </View>
        <Icon name='chevron-right' size={130} color="#003366" />
    </View>
  );
}
}


const styles = StyleSheet.create({
    container:{
        backgroundColor:'#B4E9FF',
        width:'auto',
        height:130,
        marginVertical:10,
        borderRadius:8,
        flexDirection:'row',
        alignItems:'center',
        elevation:4
    }
});