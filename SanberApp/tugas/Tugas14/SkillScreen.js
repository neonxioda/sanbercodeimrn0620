import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'; 
import SkillDataTemplate from './SkillData';
import SkillData from './skillData.json';

export default class App extends Component{
    render(){
    return (
    <View style={styles.container}>
        <Image source={require('../Tugas 13/Images/logo.png')} style={{width:187, height:51, alignSelf:'flex-end'}} />        
        <View style={styles.profile}>
            <Icon name="account-circle" size={40} color='#3EC6FF'  />
            <View style={styles.profileInfo}>
                <Text style={{fontSize:12}}>Hai !</Text>
                <Text style={{fontSize:16}}>Agung Budi Miantoro</Text>
            </View>
        </View>
            <Text style={{fontSize:36}}>SKILL</Text>
            <View style={styles.garis}></View>
            <View style={styles.kategori}>
                <View style={styles.kategoriIsi}>
                    <Text style={styles.kategoriIsiText}>Library / Framework</Text>
                </View>
                <View style={styles.kategoriIsi}>
                    <Text style={styles.kategoriIsiText}>Bahasa Pemrograman</Text>
                </View>
                <View style={styles.kategoriIsi}>
                    <Text style={styles.kategoriIsiText}>Teknologi</Text>
                </View>
            </View>
                {/* <SkillDataTemplate isi={SkillData.items[0]} /> */}
                <FlatList data={SkillData.items} renderItem={(isi)=><SkillDataTemplate isi={isi.item} />} keyExtractor={(item)=>item.id} />
    </View>
    );

}
}

const styles = StyleSheet.create({
     container: {
        flex: 1,
        margin:19
      },
      profile:{
          flexDirection:'row',
         marginTop:7,
         marginBottom:16
      },
      profileInfo:{
         marginLeft:5
      },
      garis:{
          marginVertical:16,
          width:'auto',
          height:4,
          backgroundColor:'#3EC6FF',
      },
      kategori:{
          justifyContent:'space-between',
          flexDirection:'row',
      },
      kategoriIsi:{
          height:32,
          backgroundColor:'#B4E9FF',
          borderRadius:8,
          alignItems:'center',
          marginHorizontal:3,
          padding:8
          
      },
      kategoriIsiText:{
          fontSize:12,
          color:'#003366',
          textAlign:'center',
          fontWeight:'bold'
          
      }
});