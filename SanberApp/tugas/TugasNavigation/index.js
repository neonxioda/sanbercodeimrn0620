import * as React from 'react';
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Login from './loginScreen';
import About from './aboutScreen';
import Skill from './SkillScreen';
import AddScreen from './AddScreen';
import ProjectScreen from './projectScreen';

const DrawerLogin = createDrawerNavigator();
const LoginStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const AboutStack = createStackNavigator();
const SkillStack = createStackNavigator();
const AddScreenStack = createStackNavigator();
const ProjectScreenStack = createStackNavigator();

const LoginTabs = () => (
<LoginStack.Navigator>
        <LoginStack.Screen name="Login" component={Login}/>
</LoginStack.Navigator>
);

const AboutStackScreen = () => (
    <AboutStack.Navigator>
        <AboutStack.Screen name="About" component={About} />
    </AboutStack.Navigator>
);

const SkillStackScreen = () => (
    <SkillStack.Navigator>
        <SkillStack.Screen name="Skill" component={Skill} />
    </SkillStack.Navigator>
);

const ProjectScreenStackScreen = () => (
    <ProjectScreenStack.Navigator>
        <ProjectScreenStack.Screen name="Project Screen" component={ProjectScreen} />
    </ProjectScreenStack.Navigator>
);
const AddScreenStackScreen = () => (
    <AddScreenStack.Navigator>
        <AddScreenStack.Screen name="Add Screen" component={AddScreen} />
    </AddScreenStack.Navigator>
);


const TabsBottom = () => (
<Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillStackScreen}/>
        <Tabs.Screen name="Add" component={AddScreenStackScreen}/>
        <Tabs.Screen name="Project" component={ProjectScreenStackScreen}/>
</Tabs.Navigator>
);

export default () => (
    <NavigationContainer>
        <DrawerLogin.Navigator>
            <DrawerLogin.Screen name="Login" component={LoginTabs} />
            <DrawerLogin.Screen name="About" component={AboutStackScreen} />
            <DrawerLogin.Screen name="Tabs" component={TabsBottom} />
        </DrawerLogin.Navigator>
    </NavigationContainer>
    );
    
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
