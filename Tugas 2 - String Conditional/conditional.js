//soal nomor 1
var nama = 'agung';
var peran = '';

if (nama == '') {
    console.log('nama harus diisi');
}

if (peran == '' && nama) {
    console.log('hallo ' + nama + ' Pilih peranmu untuk memulai game');
}

if (peran == 'Penyihir') {
    console.log('Selamat datang di Dunia Werewolf ' + nama);
    console.log('Halo Penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!');
} else if (peran == 'Guard') {
    console.log('Selamat datang di Dunia Werewolf ' + nama);
    console.log('Halo Guard ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.');
} else if (peran == 'Werewolf') {
    console.log('Selamat datang di Dunia Werewolf ' + nama);
    console.log('Halo Werewolf ' + nama + ', Kamu akan memakan mangsa setiap malam.');
}

/////////////////////////////////////////////////////////////////////////////////////

//soal nomor 2

var tanggal = 21;
var bulan = 12;
var tahun = 1994;

switch (bulan) {
    case 1: {
        console.log(tanggal + ' Januari ' + tahun);
        break;
    }
    case 2: {
        console.log(tanggal + ' Februari ' + tahun);
        break;
    }
    case 3: {
        console.log(tanggal + ' Maret ' + tahun);
        break;
    }
    case 4: {
        console.log(tanggal + ' April ' + tahun);
        break;
    }
    case 5: {
        console.log(tanggal + ' Mei ' + tahun);
        break;
    }
    case 6: {
        console.log(tanggal + ' Juni ' + tahun);
        break;
    }
    case 7: {
        console.log(tanggal + ' Juli ' + tahun);
        break;
    }
    case 8: {
        console.log(tanggal + ' Agustus ' + tahun);
        break;
    }
    case 9: {
        console.log(tanggal + ' September ' + tahun);
        break;
    }
    case 10: {
        console.log(tanggal + ' Oktober ' + tahun);
        break;
    }
    case 11: {
        console.log(tanggal + ' November ' + tahun);
        break;
    }
    case 12: {
        console.log(tanggal + ' Desember ' + tahun);
        break;
    }
}