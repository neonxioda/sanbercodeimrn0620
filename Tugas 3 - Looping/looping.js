// soal 1

var flag = 0;
var direction = 'maju';
console.log('LOOPOING PERTAMA');
while (flag <= 20 && direction == 'maju') {
    flag = flag + 2;
    console.log(flag + ' - I love Coding');
    if (flag == 20) {
        var direction = 'mundur';
        console.log('LOOPING KEDUA');
    }
    while (flag > 0 && direction == 'mundur') {
        console.log(flag + ' - I will become a mobile developer');
        flag = flag - 2;
    }
}

//////////////////////////////////////////////////////

// soal nomor 2

for (i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
        console.log(i + ' - I Love Coding');
    } else if (i % 2 == 0) {
        console.log(i + ' - Berkualitas');
    } else if (i % 2 == 1) {
        console.log(i + ' - Santai');
    }
}

/////////////////////////////////////////////////////////

// soal nomor 3


for (b = 0; b < 4; b++) {
    var data = '';
    for (i = 0; i < 8; i++) {
        var data = data + '#';
    }
    console.log(data);
}

/////////////////////////////////////////////////////////

// soal nomor 4

var string = '';
for (index = 0; index < 7; index++) {
    var string = string + '#';
    console.log(string);
}

/////////////////////////////////////////////////////////

// soal nomor 5

for (a = 1; a <= 8; a++) {
    var data = '';
    for (i = 1; i <= 8; i++) {
        if ((i + a) % 2 == 1) {
            var data = data + '#';
        } else {
            var data = data + ' ';
        }
    }
    console.log(data);
}