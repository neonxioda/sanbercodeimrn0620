function range(startNum, finishNum) {
    //kondisi jika parameter kosong
    if (!startNum || !finishNum) {
        return -1;
    }
    var data = []
    if (startNum < finishNum) {
        //perulangan ketika angka awal > angka akhir
        for (i = startNum; i <= finishNum; i++) {
            data.push(i);
        }
        return data;
    } else if (startNum > finishNum) {
        //perulangan ketika angka awal < angka akhir
        for (i = startNum; i >= finishNum; i--) {
            data.push(i);
        }
        return data;
    }
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

///////////////////////////////////////////////////////////////////

//soal nomor 2

function rangeWithStep(startNum, finishNum, step) {
    //kondisi jika parameter kosong.
    if (!startNum || !finishNum || !step) {
        return -1;
    }
    var data = []
    if (startNum < finishNum) {
        //perulangan ketika angka awal > angka akhir
        for (i = startNum; i <= finishNum; i = i + step) {
            data.push(i);
        }
        return data;
    } else if (startNum > finishNum) {
        //perulangan ketika angka awal < angka akhir
        for (i = startNum; i >= finishNum; i = i - step) {
            data.push(i);
        }
        return data;
    }
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

///////////////////////////////////////////////////////////////////
//soal nomor 3

function sum(startNum, finishNum, step = 1) {
    //kondisi jika parameter kosong = 0
    if (!startNum && !finishNum) {
        return 0;
    }
    // kondisi jika parameter awal saja yang ada.
    if (startNum && !finishNum) {
        return startNum;
    }
    var data = 0;
    if (startNum < finishNum) {
        //perulangan ketika angka awal > angka akhir
        for (i = startNum; i <= finishNum; i = i + step) {
            data = data + i;
        }
        return data;
    } else if (startNum > finishNum) {
        //perulangan ketika angka awal < angka akhir
        for (i = startNum; i >= finishNum; i = i - step) {
            data = data + i;
        }
        return data;
    }
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

///////////////////////////////////////////////////////////////////

// soal nomor 4

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(input) {
    for (i = 0; i < input.length; i++) {
        console.log('Nomor ID: ' + input[i][0]);
        console.log('nama Lengkap: ' + input[i][1]);
        console.log('TTL: ' + input[i][2] + ' ' + input[i][3]);
        console.log('Hobi: ' + input[i][4]);
        console.log('')
    }
}
dataHandling(input);


///////////////////////////////////////////////////////////////////

// soal nomor 5

function balikKata(kata) {
    var data = '';
    for (i = kata.length - 1; i >= 0; i--) {
        var data = data.concat(kata[i]);
    }
    return data;
}
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 


///////////////////////////////////////////////////////////////////

// soal nomor 6

function dataHandling2(input) {
    input.splice(1, 1, 'Roman Alamsyah Elsharawy');
    input.splice(2, 1, 'Provinsi Bandar Lampung');
    input.splice(4, 0, 'Pria');
    input.splice(5, 1, 'SMA Internasional Metro');
    console.log(input);

    //melakukan pemecahan pada tanggal lahir
    var tanggal = input[3].split('/');

    // melakukan pengecekan bulan
    var bulan = tanggal[1];
    switch (bulan) {
        case '01': {
            console.log('Januari');
        }
        case '02': {
            console.log('Februari');
            break;
        }
        case '03': {
            console.log('Maret');
            break;
        }
        case '04': {
            console.log('April');
            break;
        }
        case '05': {
            console.log('Mei');
            break;
        }
        case '06': {
            console.log('Juni');
            break;
        }
        case '07': {
            console.log('Juli');
            break;
        }
        case '08': {
            console.log('Agustus');
            break;
        }
        case '09': {
            console.log('September');
            break;
        }
        case '10': {
            console.log('Oktober');
            break;
        }
        case '11': {
            console.log('November');
            break;
        }
        case '12': {
            console.log('Desember');
            break;
        }
    }
    // menggabungkan tanggal dengan join
    var gabungTanggal = tanggal.join('-');
    // melakukan sorting pada tanggal YYYY,mm,dd
    var sortirTanggal = tanggal.sort(function (value1, value2) {
        return value2 - value1
    });
    // melakukan potongan pada nama dari kaa ke 0-15
    var nama = input[1];
    var irisanNama = nama.slice(0, 15);

    console.log(sortirTanggal);
    console.log(gabungTanggal);
    console.log(irisanNama);

}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

dataHandling2(input);