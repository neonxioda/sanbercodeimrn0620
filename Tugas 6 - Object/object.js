// soal nomor 1

function arrayToObject(arr) {
    var now = new Date();
    var thisYear = now.getFullYear();
    for (i = 0; i < arr.length; i++) {
        var brith = arr[i][3]
        if (brith < thisYear) {
            var age = thisYear - brith;
        } else {
            var age = 'Invalid birth year';
        }
        var data = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: age
        }
        console.log(i + 1 + '. ' + data.firstName + ' ' + data.lastName + ' : ');
        console.log(data);
    }
}

var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people);

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
arrayToObject([]);

//////////////////////////////////////////////////////////////////////////////////

//soal nomor 2

function shoppingTime(memberId = '', money = 0) {
    if (memberId == '') {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup';
    }
    var listPurchased = [];

    if (money >= 1500000) {
        listPurchased.push('Sepatu brand Stacattu');
        var changeMoney = money - 1500000;
    } else if (money >= 500000) {
        listPurchased.push('Baju brand Zoro');
        var changeMoney = money - 500000;
    } else if (money >= 250000) {
        listPurchased.push('Baju brand H&N');
        var changeMoney = money - 250000;
    } else if (money >= 175000) {
        listPurchased.push('Sweater brand Uniklooh');
        var changeMoney = money - 175000;
    } else if (money >= 50000) {
        listPurchased.push('Casing Handphone');
        var changeMoney = money - 50000;
    }
    if (money >= 1500000 && listPurchased.includes('Sepatu brand Stacattu') == false) {
        listPurchased.push('Sepatu brand Stacattu');
        var changeMoney = changeMoney - 1500000;
    }
    if (money >= 500000 && listPurchased.includes('Baju brand Zoro') == false) {
        listPurchased.push('Baju brand Zoro');
        var changeMoney = changeMoney - 500000;
    }
    if (money >= 250000 && listPurchased.includes('Baju brand H&N') == false) {
        listPurchased.push('Baju brand H&N');
        var changeMoney = changeMoney - 250000;
    }
    if (money >= 175000 && listPurchased.includes('Sweater brand Uniklooh') == false) {
        listPurchased.push('Sweater brand Uniklooh');
        var changeMoney = changeMoney - 175000;
    }
    if (money >= 50000 && listPurchased.includes('Casing Handphone') == false) {
        listPurchased.push('Casing Handphone');
        var changeMoney = changeMoney - 50000;
    }

    var data = {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: changeMoney
    };
    return data;
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//////////////////////////////////////////////////////////////////////////////////

//soal nomor 3

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var data = [];
    for (a = 0; a < arrPenumpang.length; a++) {
        var berangkat = rute.indexOf(arrPenumpang[a][1]);
        var turun = rute.indexOf(arrPenumpang[a][2]);
        var bayar = (turun - berangkat) * 2000;
        data.push({
            penumpang: arrPenumpang[a][0],
            naikDari: arrPenumpang[a][1],
            tujuan: arrPenumpang[a][2],
            bayar: bayar
        });
        // console.log(bayar);
    }
    return data;
}
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
console.log(naikAngkot([])); //[]