var readBooks = require('./callback.js')
var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
];

var time = 10000;

function baca(time, books, i) {
    if (i < books.length) {
        readBooks(time, books[i], function (callback) {
            i++
            time = callback;
            baca(time, books, i);
        });
    }
};

baca(time, books, 0);