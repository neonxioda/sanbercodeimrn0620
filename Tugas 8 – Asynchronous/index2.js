var readBooksPromise = require('./promise.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 


var time = 10000;

function baca(time, books, i) {
    if (time > 0 && i < books.length) {
        readBooksPromise(time, books[i])
            .then(function (hasil) {
                i++
                time = hasil;
                baca(time, books, i);
            }).catch(function (error) {})
    }
}

baca(time, books, 0);